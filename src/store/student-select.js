
export default {
    state: {
        selected: []
    },  
    mutations: {
        SELECT(state, student) {
            // state.selected.push(student);
            state.selected = [...state.selected, student];
            
        },
        DESELECT(state, student) {
            state.selected = 
            state.selected.filter(item => item.id !== student.id);
        },
        MODIFY(state, students) {
            state.selected = students;
        }
    },
    actions: {
        
    },
    namespaced: true
}