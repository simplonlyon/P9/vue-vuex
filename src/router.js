import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import ManagePromo from './views/ManagePromo.vue'
import ManageStudents from './views/ManageStudents.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/manage-promo',
      name: 'manage-promo',
      component: ManagePromo
    },
    {
      path: '/manage-students',
      name: 'manage-students',
      component: ManageStudents
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
