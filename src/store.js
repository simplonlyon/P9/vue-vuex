import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios';
import jwt_decode from "jwt-decode";
import studentSelect from './store/student-select';

Vue.use(Vuex)

const apiUrl = 'http://localhost:8000';

export default new Vuex.Store({
  state: {
    //Ici, l'état global des données de l'application
    user: null,
    promos: [],
    students: []
  },
  mutations: {
    //Ici, les méthodes de modification pure des données
    CHANGE_USER(state, user) {
      state.user = user;
    },
    ADD_PROMO(state, promo) {
      state.promos.push(promo);
    },
    REMOVE_PROMO(state, promo) {
      state.promos = state.promos.filter(item => item.id !== promo.id);
    },
    MODIFY_PROMOS(state, promos) {
      state.promos = promos;
    },
    MODIFY_STUDENTS(state, students) {
      state.students = students;
    },
    UPDATE_PROMO(state, promo) {
      //inutile
      for(let x = 0; x < state.promos.length; x++) {
        if(state.promos[x].id === promo.id) {
          state.promos[x]= promo;
        }
      }
    }
  },
  actions: {
    //Ici, les méthode asynchrone notamment
    changeUser({ commit }, user) {
      //On ne peut commit que les nom de méthodes qui existent dans mutations
      commit('CHANGE_USER', user);

    },
    /**
     * @param {{username: string, password: string}} credentials 
     */
    async login({ commit }, credentials) {
      //L'action va d'abord faire un appel ajax faire l'api rest
      let response = await Axios.post(apiUrl+'/api/login_check', credentials);
      //Si ça marche, on stock le token dans le localStorage
      localStorage.setItem('token', response.data.token);
      //Et on lance un commit pour changer l'état de notre store
      commit('CHANGE_USER', credentials.username);
    },
    logout({ commit }) {
      //A la déconnexion, on dégage le token du localStorage
      localStorage.removeItem('token');
      //Et on change l'état de notre user dans le store
      commit('CHANGE_USER', null);
    },
    checkToken({ commit }) {
      if (localStorage.getItem('token')) {
        let token = jwt_decode(localStorage.getItem('token'));
        if (new Date().getTime() < token.exp * 1000) {
          commit('CHANGE_USER', token.username);
        }else {
          localStorage.removeItem('token');
        }
      }
    },
    async fetchPromos({commit}) {
      let response = await Axios.get(apiUrl+'/api/promo');
      commit('MODIFY_PROMOS', response.data);
    },
    async addPromo({commit}, promo) {
      let response = await Axios.post(apiUrl + '/api/promo', promo);
      commit('ADD_PROMO', response.data);
    },
    async fetchStudents({commit}) {
      let response = await Axios.get(apiUrl+'/api/user');
      commit('MODIFY_STUDENTS', response.data);
    },
    async updatePromo({commit, state, dispatch}, promo) {
      let data = Object.assign({}, promo);
      data.students = state.studentSelect.selected.map(item => item.id);
      let response = await Axios.patch(apiUrl+'/api/promo/'+promo.id,data);
      // commit('UPDATE_PROMO', response.data);
      dispatch('fetchPromos')
      .then(() => commit('studentSelect/MODIFY', []));
    }
  },
  strict: true,
  modules: {
    studentSelect
  }
})
